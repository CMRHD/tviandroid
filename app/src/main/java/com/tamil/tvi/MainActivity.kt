package com.tamil.tvi

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.devbrackets.android.exomedia.listener.OnErrorListener
import com.devbrackets.android.exomedia.listener.OnPreparedListener
import com.devbrackets.android.exomedia.ui.widget.VideoView
import java.lang.Exception


class MainActivity : AppCompatActivity(), OnPreparedListener ,OnErrorListener {

    // url of video which we are loading.
    lateinit var videoView: VideoView
    lateinit var progressbar : ProgressBar
    var videoURL = "https://live.tamilvision.tv/TVI/HD/playlist.m3u8"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main)
        videoView = findViewById<View>(R.id.video_view) as VideoView
        progressbar = findViewById(R.id.progress_bar)
        videoView.setOnPreparedListener(this)
        videoView.setOnErrorListener(this)
        showHide(progressbar)
        //For now we just picked an arbitrary item to play
        videoView.setVideoURI(Uri.parse(videoURL))


    }
    override fun onPrepared() {
        //Starts the video playback as soon as it is ready
        showHide(progressbar)
        videoView.start()
    }

    private fun showHide(view:View) {
        view.visibility = if (view.visibility == View.VISIBLE){
            View.INVISIBLE
        } else{
            View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        videoView.start()
    }

    override fun onPause() {
        super.onPause()
        videoView.pause()
    }

    override fun onError(e: Exception?): Boolean {
        videoView.pause()
        videoView.setOnPreparedListener(this)
        videoView.setOnErrorListener(this)
        showHide(progressbar)
        //For now we just picked an arbitrary item to play
        videoView.setVideoURI(Uri.parse(videoURL))
        return false
    }


}